# Server-utils
utilities for servers

## Virtual Machines
Scripts to automate interactions with the virtual machines from my hypervisor

### Virt-clone.sh
A script to automate the creation of virtal machines. It simplifies the virt-clone utility by making
a wizard. Might have to run as root depending on your setup. Requires libvirtd.

Tested and written on ubuntu 16.04.

## Network
Scripts to automate network setup for new VMs.

### Static.sh
A script to automate the initialtion of freshly cloned VMs. It sets the static IP and hostname. It
has my default for DNS, gateway etc. so modification to the code might be required depending on your
setup.

Tested and written on ubuntu server 16.04 and needs to be run as root.

TODO: add a check to see if run as root.

