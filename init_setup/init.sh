#!/bin/bash

# This script wil set hostname, let you select apt sources
# and update the software. Use this after cloning a new machine

if [ "$EUID" -ne 0 ] 			# Check if user is root
  then echo "Please run as root"
  exit
fi

read -p "Enter a new hostname: " hostname
read -p "Do you want multiverse, universe or none [m/u/n]: " sources

hostnamectl set-hostname $hostname

if [ $sources == n ]; then
	cat << EOF > /etc/apt/sources.list
	deb http://archive.ubuntu.com/ubuntu bionic main
	deb http://archive.ubuntu.com/ubuntu bionic-security main
	deb http://archive.ubuntu.com/ubuntu bionic-updates main
EOF

elif [ $sources == u ]; then
	cat << EOF > /etc/apt/sources.list
	deb http://archive.ubuntu.com/ubuntu bionic main universe
	deb http://archive.ubuntu.com/ubuntu bionic-security main universe
	deb http://archive.ubuntu.com/ubuntu bionic-updates main universe
EOF

elif [ $sources == m ]; then
	cat << EOF > /etc/apt/sources.list
	deb http://archive.ubuntu.com/ubuntu bionic main multiverse universe
	deb http://archive.ubuntu.com/ubuntu bionic-security main multiverse universe
	deb http://archive.ubuntu.com/ubuntu bionic-updates main multiverse universe
EOF
fi

read -p "Do you wish to set static IP? [y/N]" opt

if [ $opt == y ]; then
	if [ -f /etc/network/interfaces ]; then
		if [ -f ../Network/static.sh ]; then
			../Network/static.sh
		else
			echo "'Network/static.sh' not found"
		fi
	else
		echo "'/etc/network/interfaces not found'"

	fi
fi

read -p "Do you wish to reboot after updating? [y/N]" opt

apt update && apt upgrade


if [ $opt == y ]; then
	reboot
fi
