#!/bin/bash

#This is a script to clone a new virtual machine from a template vm.

# Some constant variables. bootPool is where you want to store files
# origVM is the vm you want to clone. ubu16-clone-1 is my template vm.
bootPool=/fastdata/virt\ drives/
origVM=ubu16-clone-1

# SSH variables
ip=10.0.0.69 # the IP for the template VM.
user=eirik # template VM user
init='sudo sh /home/eirik/static.sh; sudo reboot'


# Grabs the name for the new VM.
getinfo(){
    read -p "Enter the new domain for your server: (UbuV-role-01) " domain
    
#check if domain exist 
    domainEx="$(virsh list --all | grep $domain)"
    echo $domainEx
    while [ -n "$domainEx" ]; do
    read -p "This domain appear to exist. Do you want to run [i]nit ssh, [r]ename, [e]xit or [f]orce?
    [i/r/e/f]: " iref
	case $iref in
	    [Ii]* ) echo "Starting ssh session and starting init script. Please wait."
		    ssh -t $user@$ip "$init";;
	    [Rr]* ) echo "Retrying"
		    getinfo;;
	    [Ee]* ) echo "Exiting...."
		    exit 0;;
	    [Ff]* ) return 1;;
		* ) echo "Please enter y, n or e";;
	esac
    done
}

# Asks if you want to clone the template VM and runs the main function. If not enter a 
# different VM name.
origVM(){
    while true; do
    read -p "is this the VM you want to clone? ($origVM) [y/n]:" yn
	case $yn in
	    [Yy]* ) main;;
	    [Nn]* ) read -p "Enter other domain " origVM
		    main;;
		* ) echo "Please enter y or n";;
	esac
    done
}



# Main function that will run the final bit of code.
main(){
    echo ""
    echo "Virt drive: $bootPool$domain"
    echo "Original VM: $origVM"
    echo "virt-clone -n $domain -f "$bootPool"$domain -o $origVM"
    virt-clone -n $domain -f "$bootPool"$domain -o $origVM
    echo ""
    echo "Starting $domain"
    setup
    echo ""
    while true; do
	read -p "Do you wish to start the domain and ssh in to launch initiation script? [y/n]: " yn
	case $yn in
	[Yy]* ) echo "Starting machine and ssh session and starting init script. Please wait."	
		virsh start $domain
		ssh -t $user@$ip "$init"
		exit 0;;
	[Nn]* ) echo "New guest's IP is : $ip"
		exit 0;;
	    * ) echo "Please enter y or n";;
	esac
    done

    sshFunc
}

setup(){
    while true; do
	read -p "Do you wish to alter the domain's settings? [y/n]: " yn
	case $yn in
	[Yy]* )	wantmem
		wantCPU
		return 1;;
	[Nn]* ) return 1;;
	    * ) echo "please enter y or n";;
	esac
    done
    
}

wantmem(){
    while true; do
	read -p "Do you want to increase the memory for $domain? [y/n]: " yn
	case $yn in
	    [Yy]* ) setmem
		    return 1;;
	    [Nn]* ) return 1;;
		* ) echo "Please enter y or n";;
	esac
    done 
}

setmem(){
    read -p "how much memory do you want? (default 512MiB) [MiB]: " mem
    case $mem in
    [0-9]* )	sudo virsh setmaxmem --domain $domain --size "$mem"M --config
		sudo virsh setmem --domain $domain --size "$mem"M --config
		return 1;;
	    * ) echo "Please enter a number"
		setmem;;
    esac
}

wantCPU(){
    while true; do
	read -p "Do you want to increase the amount of cores for $domain? [y/n]: " yn
	case $yn in
	    [Yy]* ) setCPU
		    return 1;;
	    [Nn]* ) return 1;;
		* ) echo "Please enter y or n";;
	esac
    done 
}

setCPU(){
    read -p "how many cores do you want? (default 1) [0-9]: " cpu
    case $cpu in
    [0-9]* )	sudo virsh setvcpus --domain $domain --maximum "$cpu" --config
		sudo virsh setvcpus --domain $domain --current "$cpu"
		return 1;;
	    * ) echo "Please enter a number"
		setCPU;;
    esac
}

getinfo
origVM
