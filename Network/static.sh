#!/bin/bash

netmask=255.255.255.0
gateway=10.0.0.1
dns=10.0.0.1

getinfo()
{
  read -p "Enter the ip address for your server: (looks like 192.168.1.22)  " staticip
  read -p "Enter the hostname for your server: (Ubu16-role-01): " hostname
}

writeinterfacefile()
{ 
cat << EOF > $1 
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).
# The loopback network interface
auto lo
iface lo inet loopback

#Your static network configuration  
auto ens3
iface ens3 inet static
address $staticip
netmask $netmask
gateway $gateway 
dns-nameserver $dns
EOF

  echo ""
  echo "Your informatons was saved in '$1' file."
  echo ""

}


writehostsfile()
{ 
cat << EOF > $file2
127.0.0.1       localhost
127.0.1.1       $hostname

# The following lines are desirable for IPv6 capable hosts
::1     localhost ip6-localhost ip6-loopback
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
EOF

  echo ""
  echo "Your informatons was saved in '$file2' file."
  echo ""
  exit 1
}

file2="/etc/hosts"
if [ ! -f $file2 ]; then
  echo ""
  echo "The file '$file2' doesn't exist!"
  echo ""
  exit 1
fi



file="/etc/network/interfaces"
if [ ! -f $file ]; then
  echo ""
  echo "The file '$file' doesn't exist!"
  echo ""
  exit 1
fi



getinfo
echo ""
echo "So your settings are:"
echo "Your decided Server IP is:   $staticip"
echo "Your gateway is:	$gateway"
echo "Your netmask is:	$netmask"
echo "Your dns is:	$dns"
echo "Your hostname is:	$hostname"
echo "Please restart your pc to apply the settings"




echo ""

while true; do
  read -p "Are these informations correct? [y/n]: " yn 
  case $yn in
    [Yy]* ) writeinterfacefile $file && hostnamectl set-hostname $hostname && writehostsfile $file ;;
    [Nn]* ) getinfo;;
        * ) echo "Pleas enter y or n!";;
  esac
done
